Hello,
My name is Stephen Richmond and I'm a Graduate Research Assistant in Computer Science.  I've been working for ICL on campus for 
almost 2 1/2 years now and I've also done collaborative work at ORNL.  Obviously, my interests lie in high performance computing,
but I'm fascinated by every field of CS... except web development.
Good Luck in the Class,
Stephen