I'm Drew Schmidt, a quasi-phd student. I have a masters in number theory
from the University of Tennessee. I've spent the last ~5 years working
in various roles, including statistical consultant and HPC programmer.
